# Samples.ServiceLayer-ServiceFacade-RemoteAPI

HolisticWare Team

[http://holisticware.net](http://holisticware.net)

## ASP.net service layer (facade, RemoteAPIs) samples

### SignalR + WebAPI

#### HolisticWare.WebApp.MVC4RazorWebAPISignalR-1x

Technology:

1.	ASP.net MVC4
2.	SignalR 1.x
3.	WebAPI

Platforms

1. 	IIS  
	1.	[]()

### REST

TODO.

### XML-RPC

TODO.

### SOAP Web Services

TODO.
